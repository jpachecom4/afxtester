ON/OFF the machine.

Set the Steeringwheel.
wait 2 sec.
clamp.

Operator put  the quick conector in the steeringwheel.
start cycle (PB).

IF OK green greenlightON. 
mark the piece.
unclamp.
DONE.

ELSE red light ON.
wait reset.
unclamp.


INPUTS(X):
X0	MainSwithc
X1	EmergencyStop
X2	PartPresent
X3	PB
X4	ResetKey
X5	LMS1
X6	OK_Signal
X7	NOK_Signal

OUTPUTS(Y):
Y0	MachineON
Y1	Clamps
Y2	MarkerOK
Y3	GreenLight
Y4	RedLight

BITS(C):

Timers[TMR,K(Preset)]
T0	DelayToClamp

Counters[CTR,K(Preset)]
