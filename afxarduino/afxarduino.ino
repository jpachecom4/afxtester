/*
AFX Tester
Made By Javier Pacheco.
*/ 

//Strings to printer!!
#include <SPI.h>

//Here are some declarations for pin and variables, etc.
int pinSigOK = 2;
int led = 13;
int valueOK;
String testerValue;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(pinSigOK,INPUT);
}

void loop() {
  valueOK = digitalRead(2); // Read the value of PIN2 1=TRUE, 0=FALSE
  if (valueOK==1){
    testerValue = Serial.readStringUntil('\n');
    if (testerValue == NULL){
      return; 
    }
    digitalWrite(led,HIGH);
    Serial.println("^XA");    // Start
    Serial.println("^MMP");   // Config
    Serial.println("^PW280"); //Label width set to 280mm
    Serial.println("^LL120"); //Label size set to 120mm
    Serial.println("^LS0");   //Left Padding set to 0
    Serial.println("^XZ");
    Serial.println(testerValue);
    delay(1000);
    valueOK = 0;
    digitalWrite(led,LOW);
  }
  valueOK = 0;
}
